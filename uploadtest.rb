require 'rubygems'
require 'httpclient'
require 'json'



url_str = "https://upload.box.com/api/2.0/files/content"


def send_huge_file(url_str)
  
  puts "Type in your Authentication token and press enter"
  authtoken = gets.chomp!
  puts "Type in the folder ID you want to upload to and press enter"
  folder_id = gets.chomp!
  puts "which test would you like to perform?"
  puts "A. 7KB    B. 50KB     C. 5MB    D. 100MB    E. 5GB (do not use)"
  testcase = gets.chomp!
  
  case testcase
  when "A" then
    num = 1
     testing = "7K test "
    file_path = "./upfile/7ktest.txt"
    while num < 500 do
    puts "performing upload test #{num}"
    res = uploadtest(authtoken,file_path, num, folder_id, url_str, testing)
    fileid = res["entries"][0]["id"]
    puts "performing delete file #{num}"
    deletefile(authtoken,fileid)
    num = num + 1
    end
  when "B" then
    num = 1
    testing = "50K test "
    file_path ="./upfile/50ktest.txt"
    while num < 500 do
    puts "performing upload test #{num}"
    res = uploadtest(authtoken,file_path, num, folder_id, url_str, testing)
    fileid = res["entries"][0]["id"]
    puts "performing delete file #{num}"
    deletefile(authtoken,fileid)
    num = num + 1
    end
  when "C" then
    num = 1
    testing = "5M test "
    file_path ="./upfile/5Mtest.txt"
    while num < 500 do
    puts "performing upload test #{num}"
    res = uploadtest(authtoken,file_path, num, folder_id, url_str, testing)
    fileid = res["entries"][0]["id"]
    puts "performing delete file #{num}"
    deletefile(authtoken,fileid)
    num = num + 1
    end
  when "D" then
    num = 1
    testing = "100M test "
    file_path ="./upfile/100Mtest.txt"
    while num < 500 do
    puts "performing upload test #{num}"
    res = uploadtest(authtoken,file_path, num, folder_id, url_str, testing)
    fileid = res["entries"][0]["id"]
    puts "performing delete file #{num}"
    deletefile(authtoken,fileid)
    num = num + 1
    end
  when "E" then
    num = 1
    testing = "5GB test "
    file_path ="./upfile/5GBtest.txt"
    while num < 10 do
    puts "performing upload test #{num}"
    res = uploadtest(authtoken,file_path, num, folder_id, url_str, testing)
    fileid = res["entries"][0]["id"]
    puts "performing delete file #{num}"
    deletefile(authtoken,fileid)
    num = num + 1
    end
  else
    puts "I dont know what you said"
    end
end

def uploadtest(authtoken, file_path, num, folder_id, url_str, testing)

  boundary = "boundary"
  client = HTTPClient.new
  client.connect_timeout = 3660
  client.send_timeout    = 3660
  client.receive_timeout = 3660
  header = {"Authorization" => "Bearer #{authtoken}" }
  puts header
  begin
  File.open(file_path) do |io|
    postdata = {
      "filename" => io,
	   "folder_id" => folder_id,
    }
    #Get begin time
    t1 = Time.now()
    outcome = client.post_content(url_str, postdata,header, {
      "content-type" => "multipart/form-data; boundary=#{boundary}",
    },)
    #get end time
	  t2 = Time.now()
    parsed = JSON.parse(outcome)
    #t2 - t1 = total time
    tt = t2 - t1
    open('./results/uploadResults.txt', 'a') do |f|
      f << "#{testing}, Test: #{num}:, #{tt}, #{t2}, #{t1} \n"
          end
              return parsed
        end
    end
end

def deletefile(authtoken, fileid)
  url = "https://api.box.com/2.0/files/#{fileid}"
  client = HTTPClient.new
  header = {"Authorization" => "Bearer #{authtoken}" }
  client.delete(url,nil,header)
end

send_huge_file(url_str)
